package com.example.restservice;

public class Greeting {

	private final long id;
	private final String content;
	private long length;

	public Greeting(long id, String content) {
		this.id = id;
		this.content = content;
	}

	public Greeting(long id, String content, long length) {
		this.id = id;
		this.content = content;
		this.length = length;
	}

	public long getId() {
		return id;
	}

	public String getContent() {
		return content;
	}

	public long getLength() {
		return length;
	}
}
